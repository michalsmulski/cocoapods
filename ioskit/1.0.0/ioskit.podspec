#
# Be sure to run `pod lib lint ioskit.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "ioskit"
  s.version          = "1.0.0"
  s.summary          = "A short description of ioskit."
  s.description      = "asdaskldhfbaslkjhfbkjahsfbdkjahbdf"
	s.homepage         = "http://www.google.pl"
  s.license          = 'MIT'
  s.author           = { "Michal Smulski" => "michal.smulski@boostcom.com" }
  s.source           = { :git => "https://michalsmulski@bitbucket.org/michalsmulski/ioskit.git", :tag => s.version.to_s }

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/*.swift'
# s.resource_bundles = {
#    'ioskit' => ['Pod/Assets/*.png']
#  }
end
